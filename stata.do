clear all
pause on
set more off
insheet using data.csv,comma clear names

* Track position
destring tpos,replace force
* Track listeners
destring tlisteners,replace force  
* Track playcount 
destring tplaycount,replace force

* Not used:
destring alb_listeners,replace force
destring alb_playcount,replace force

* Release id (ie, album id)
egen rid = group(aname rname)

bysort rid: gen ntracks = _N
* Drop anthologies that have a ton of tracks
replace tpos = . if ntracks > 40

* Some songs have songs with quotes in the name, which screws up the data; drop these albums
gen missing_tpos = (tpos==.)
bysort rid: egen dropme = max(missing_tpos)
keep if dropme==0
drop dropme missing_tpos


label define tposlab 1 "1" 2 "2" 3 "3" 4 "4" 5 "5" 6 "6" 7 "7" 8 "8" 9 "9" 10 "10"
label values tpos tposlab
ssc install coefplot

* Regressions
qui reg tplaycount i.rid i.tpos,robust coefleg
#delim ;
coefplot , keep(
   2.tpos
   3.tpos
   4.tpos
   5.tpos
   6.tpos
   7.tpos
   8.tpos
   9.tpos
   10.tpos
   ) 
   xline(0)
   xtitle("Playcount")
   ytitle("Track position")
   ;
#delim cr
graph export playcount.png,replace

qui reg tlisteners i.rid i.tpos,robust coefleg
#delim ;
coefplot , keep(
   2.tpos
   3.tpos
   4.tpos
   5.tpos
   6.tpos
   7.tpos
   8.tpos
   9.tpos
   10.tpos
   ) 
   xline(0)
   xtitle("Listeners")
   ytitle("Track position")
   ;
#delim cr
graph export listeners.png,replace

