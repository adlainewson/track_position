This is a quick project to scrape track popularity data from the social music website last.fm.

I (thought I) noticed that disproportionately many of my favourite songs from albums were in track position 3, so in order to test this I wrote this code to download data on the five top albums for the fifty top artists on last.fm, and check whether in fact tracks in position 3 of these albums had higher number listeners or playcounts.

The resulting dataset is about 3000 tracks across about 200 albums associated with the 50 most popular artists on last.fm.

Below are the estimated track position dummies from a regression of listeners (playcount) on track position and album dummies (this controls for the fact that albums with fewer tracks may be more popular on average).  The estimated effect of each track position is shown, along with a 95% confidence interval. The coefficients are interpreted relative to the popularity of the first track on the album. As hypothesized, track three appears more popular than all other tracks, except the first.

![](listeners.png)

![](playcount.png)

Lastfm API documentation is from here:

https://www.last.fm/api
