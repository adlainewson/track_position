# API info
#Application nam     track_numbers
#API key	    74dcbe6e67f8fb4ad914bcc34b262cd2
#Shared secret	    6b7b826ecc47a0da8fdbbb977cd4ec03
#Registered to	    mordethmai

import requests 
import json
from time import sleep
from urllib.parse import quote_plus

apikey = '74dcbe6e67f8fb4ad914bcc34b262cd2'
base = 'http://ws.audioscrobbler.com/'


top_artists_r  = requests.get(base + '/2.0/?method=geo.gettopartists&country=canada&api_key=' + apikey + '&format=json')
top_artists_json= json.loads(top_artists_r.text)

output_file = "data.csv"
with open(output_file,"a+") as ofile:
    ofile.write("aname,rname,alb_listeners,alb_playcount,tname,tpos,tlisteners,tplaycount\n")

for artist in top_artists_json['topartists']['artist']:
    n_albums = 0 # Only take the first five albums (looks like they're ranked by playcount)
    aname = artist['name']
    #aid = artist['mbid']
    alisteners = artist['listeners']
    topalbums_r = requests.get(base + '/2.0/?method=artist.gettopalbums&artist=' + quote_plus(aname) + '&api_key=' + apikey + '&format=json')
    topalbums_json = json.loads(topalbums_r.text)

    for topalbum in topalbums_json['topalbums']['album']:
        # Releases Rij
        rname = topalbum['name']
        #rid = topalbum['mbid']
        #rpc = topalbum['playcount']
        #print(rname + " by " + aname + " with mbid: " + rid)

        ## Get track positions off MBID
        #mb_album_r = requests.get('http://musicbrainz.org/ws/2/release/' + rid + '?inc=artist-credits+labels+discids+recordings&fmt=json')
        #mb_album_json = json.loads(mb_album_r.text)
        #print(mb_album_json['media'][0]['tracks'])
        #for track in mb_album_json['media'][0]['tracks']:
        #    tname = track['title']
        #    tlength = track['length']
        #    tid = track['id']
        #    tposition = track['position']
        #    tnumer = track['number'] # Not sure what the difference is between number and position
        #    print(aname + ', ' + rname + ', ' + tname)

        ### Now get playcounts from LFM
        #album_r = requests.get(base + '/2.0/?method=album.getinfo&api_key=' + apikey + '&artist=' + quote_plus(aname) + '&album=' + quote_plus(rname) + '&format=json')
        #album_json = json.loads(album_r.text)
        #for album in album_json['album']:
        #    alb_playcount = album['playcount']
        #    alb_listeners = album['listeners']
        #    for track in album['tracks']['track']
        
        n_albums += 1
        if n_albums<=5:
            try:
                # Get track names and positions from LFM
                album_r = requests.get(base + '/2.0/?method=album.getinfo&api_key=' + apikey + '&artist=' + quote_plus(aname) + '&album=' + quote_plus(rname) + '&format=json')
                album_json = json.loads(album_r.text)

                alb_playcount = album_json['album']['playcount']

                alb_listeners = album_json['album']['listeners']
                alb_name = album_json['album']['name']
                for track in album_json['album']['tracks']['track']:
                    try:
                        # Get track name, position, then look it up
                        tname = track['name']
                        tpos = track['@attr']['rank']
                        track_r = requests.get(base + '/2.0/?method=track.getinfo&api_key=' + apikey + '&artist=' + quote_plus(aname) + '&track=' + quote_plus(tname) + '&format=json')
                        track_json = json.loads(track_r.text)
                        tlisteners = track_json['track']['listeners']
                        tplaycount = track_json['track']['playcount']
                        ostring = "\"" + aname + "\",\"" + rname + "\",\"" + alb_listeners + "\",\"" + alb_playcount + "\",\"" + tname + "\",\"" + tpos + "\",\"" + tlisteners + "\",\"" + tplaycount + "\""
                        print(ostring)
                        with open(output_file,"a") as ofile:
                            ofile.write(ostring + "\n")
                    except:
                        print("Track error")
            except:
                print("Getting album error")
            sleep(10)
        
        

